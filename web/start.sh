#!/bin/bash
source /etc/apache2/envvars
if [ -z "$(ls -A /var/www/html/wordpress)" ]; then
   echo "Pas encore fait"
   curl -s -u $HTTP_SRC_LOGIN:$HTTP_SRC_PWD $HTTP_SRC_WWW > /dev/null
   curl -s -u $HTTP_SRC_LOGIN:$HTTP_SRC_PWD $HTTP_SRC_DB > /dev/null
   unzip /var/www/src/*.zip -d /var/www/html/
   sed -i 's/192.168.1.16/'$HOST_IP'/g' /var/www/src/*.sql
   sed -i 's/192.168.10.13/'$HOST_IP'/g' /var/www/src/*.sql
   #rm /var/www/src/*
   sed -i 's/localhost/db/g' /var/www/html/*/wp-config.php
   rm /var/www/html/*/.htaccess

else
   echo "C'est déjà fait"
fi
exec apache2 -D FOREGROUND
